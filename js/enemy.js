class Enemy {

    constructor(tile, id) {

        this.id = "enemy" + id;

        let hobGoblin = "hobgoblin_dagger";
        let gnoll = "gnoll_club";
        let orc = "orc_club";
        let axeKnight = "axe knight_";
        let swordKnight = "sword knight_";
       
        // Stats
        this.type = "enemy";
        this.alive = true;
        this.speed = 50;
        
        let enemyImg = new Image();
        let imgArr = [
            hobGoblin, hobGoblin, hobGoblin, hobGoblin, hobGoblin,
            gnoll, gnoll, gnoll, gnoll, gnoll, 
            orc, orc, orc, orc,
            swordKnight, 
            axeKnight
        ];
        let imgName = imgArr[$.rng(0, imgArr.length)];
        enemyImg.src = enemiesUrl + imgName  + ".png";
        imgName = imgName.substring(0, imgName.indexOf("_"));
        this.name = imgName.charAt(0).toUpperCase() + imgName.slice(1);
        this.image = enemyImg;
        
        let baseRad = 50;
        this.x = tile.x;
        this.y = tile.y;
        this.width = baseRad;
        this.height = baseRad;
        this.baseColor = "transparent";
       
        this.obstacleUp = false;
        this.obstacleDown =false;
        this.obstacleLeft = false;
        this.obstacleRight = false; 
        this.obstacleNw = false;
        this.obstacleNe = false;
        this.obstacleSe = false;
        this.obstacleSw = false;
        
        this.playerNear = false;
        this.playerUp = false;
        this.playerDown =false;
        this.playerLeft = false;
        this.playerRight = false; 
        this.playerNw = false;
        this.playerNe = false;
        this.playerSe = false;
        this.playerSw = false;

        switch (this.name) {
            case "Hobgoblin":
                this.damage = 3;
                this.hp = 10;
                this.hitChance = 0.5;
                this.xpGain = 1;
                break;
            
            case "Gnoll":
                this.damage = 5;
                this.hp = 20;
                this.hitChance = 0.5;
                this.xpGain = 2;
                break;

            case "Orc":
                this.damage = 7;
                this.hp = 30;
                this.hitChance = 0.5;
                this.xpGain = 3;
                break;
                
            case "Axe knight":
                this.damage = 20;
                this.hp = 50;
                this.hitChance = 0.5;
                this.xpGain = 10;
                break;
            
            case "Sword knight":
                this.damage = 15;
                this.hp = 50;
                this.hitChance = 0.7;
                this.xpGain = 10;
                break;
        }
    }

    moveUp() {
        if (!this.obstacleUp && !this.playerUp) {
            this.y -= this.speed;
        }
    }
    
    moveDown() {
        if (!this.obstacleDown && !this.playerDown) {
            this.y += this.speed;
        }
    }
    
    moveLeft() {
        if (!this.obstacleLeft && !this.playerLeft) {
            this.x -= this.speed;
        }
    }
    
    moveRight() {
        if (!this.obstacleRight && !this.playerRight){
            this.x += this.speed;
        }
    }
    
    moveNw() {
        if (!this.obstacleNw && !this.playerNw){
            this.y -= this.speed;
            this.x -= this.speed;
        }
    }
    
    moveNe() {
        if (!this.obstacleNe && !this.playerNe){
            this.y -= this.speed;
            this.x += this.speed;
        }
    }
    
    moveSe() {
        if (!this.obstacleSe && !this.playerSe){
            this.y += this.speed;
            this.x += this.speed;
        }
    }
    
    moveSw() {
        if (!this.obstacleSw && !this.playerSw){
            this.y += this.speed;
            this.x -= this.speed;
        }
    }

    // No obstacles next to the enemy.
    clearObstacles() {
        player.clearEnemies()
        this.obstacleUp = false;
        this.obstacleDown = false;
        this.obstacleLeft = false;
        this.obstacleRight = false;
        this.obstacleNw = false;
        this.obstacleNe = false;
        this.obstacleSe = false;
        this.obstacleSw = false;

        this.playerNear = false;
        this.playerUp = false;
        this.playerDown =false;
        this.playerLeft = false;
        this.playerRight = false; 
        this.playerNw = false;
        this.playerNe = false;
        this.playerSe = false;
        this.playerSw = false;
    }
}