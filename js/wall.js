class Wall {

    constructor(tile, id, level) {

        let dungeonWall = "brick_dark_0.png";
        let vineWall = "brick_brown-vines_4.png";

        this.id = "wall" + id;
        this.type = "wall";
        this.alive = true;
        let rad = 50;
        this.x = tile.x;
        this.y = tile.y;
        this.width = rad;
        this.height = rad;
        this.color = "gray";
        let wallImg = new Image();
        if (level <= 10) {
            wallImg.src = wallUrl + dungeonWall;
        }
        else if (level <= 20) {
            wallImg.src = wallUrl + vineWall;
        }
        this.image = wallImg;
    }   
}