class Exit {

    constructor(tile, alive, id) {
        this.id = "exit" + id;
        let rad = 45;
        this.x = tile.x;
        this.y = tile.y;
        this.width = rad;
        this.height = rad;
        this.color = "white";
        this.alive = alive;
        let exitImg = new Image(); 
        exitImg.src = imgUrl + "rock_stairs_down.png";
        this.image = exitImg;
    }
}