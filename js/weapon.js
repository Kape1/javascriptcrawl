class Weapon {

    constructor(tile, id, specificImage) {

        let dagger = "dagger_new";
        let club = "club_new";
        let longSword = "long sword_1_new";
        let heavyAxe = "heavy axe_4";

        this.id = "weapon" + id;
        this.type = "weapon";
        let gImg = new Image();
        let eImg = new Image();

        if (!tile) {
            // Unarmed weapon
            this.category = "";
            this.name = this.category;
            this.damage = 1;
            eImg.src = wpnUrl + "unarmed.png";
            this.equipImage = eImg;
            gImg.src = wpnUrl + "unarmed.png";
            this.groundImage = gImg;                    
            this.hitPercent = 50;
            this.hitChance = this.hitPercent / 100;
            this.durability = 0;
            this.maxDur = 0;
        } 
        else {
            // Different weapons
            let imgArr = [
                dagger, dagger, dagger, dagger, dagger, 
                club, club, club, club, club, 
                longSword, 
                heavyAxe
            ];

            let imgName = "";
            if (specificImage != null) {
                imgName = specificImage;
                console.log("WEAPON DROP CREATED: " + specificImage);
            }
            else {
                imgName = imgArr[$.rng(0, imgArr.length)];
            }
            
            // Ground image
            gImg.src = pickupsUrl + imgName + ".png";
            this.name = imgName.substring(0, imgName.indexOf("_"));
            this.groundImage = gImg;

            // Equip image
            eImg.src = wpnUrl + this.name + ".png";
            this.equipImage = eImg;

            // Uppercase the name
            this.name = this.name.charAt(0).toUpperCase() + this.name.slice(1);

            switch (this.name) {
                case "Dagger": 
                    this.category = "Light Weapon";
                    this.damage = 5;
                    this.hitPercent = 80;
                    this.hitChance = this.hitPercent / 100;
                    this.maxDur = 60;
                    this.durability = this.maxDur;
                    break;

                case "Club": 
                    this.category = "Heavy Weapon";
                    this.damage = 7;
                    this.hitPercent = 50;
                    this.hitChance = this.hitPercent / 100;
                    this.maxDur = 40;
                    this.durability = this.maxDur;
                    break;

                case "Long sword": 
                    this.category = "Light Weapon";
                    this.damage = 10;
                    this.hitPercent = 90;
                    this.hitChance = this.hitPercent / 100;
                    this.maxDur = 120;
                    this.durability = this.maxDur;
                    break;

                case "Heavy axe": 
                    this.category = "Heavy Weapon";
                    this.damage = 15;
                    this.hitPercent = 60;
                    this.hitChance = this.hitPercent / 100;
                    this.maxDur = 80;
                    this.durability = this.maxDur;
                    break;
            }
    
            let rad = 25;
            this.width = rad;
            this.height = rad;
            this.x = tile.x;
            this.y = tile.y;
            this.alive = true;
        }
    }
}