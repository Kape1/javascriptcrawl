"use strict"

let canvas;
let ctx;
let gameAreaX = 800, gameAreaY = 600;
let guiCanvas;
let gui;
let guiX = 800, guiY =  100;
let eventText = "";
let eventText2 = "";

let skillScreenOpen = false;

const fps = 100;
//let movementSpeed = 1; // Smaller the faster.
let level = 1;
let score = 0;
let coinAmount = 10000;
let coinsPickedUp = 0;

let backgroundColor = black;

let player;
let exit = new Exit(randomTile, false);

let tiles = new Array();
let wallArr = new Array();
let coinArr = new Array();
let potionArr = new Array();
let enemyArr = new Array();
let weaponArr = new Array();
let torsoArmorArr = new Array();

// Movement buttons.
let upButton, downButton, leftButton, rightButton,
nwButton, neButton, seButton, swButton, 
enterButton;

function init() {
    console.log(window.screen);
    console.debug("init() called!");
    canvas = $.id("gameCanvas");
    ctx = canvas.getContext("2d");
    guiCanvas = $.id("uiCanvas");
    gui = guiCanvas.getContext("2d");
    guiCanvas.width = canvas.width / 1.2;
    setUpControls();
    startGame();
    setInterval(draw, 1000/fps);
    //setInterval(moveEnemies, 100000/ fps);

    // Hide button controls on pc
    if (window.screen.availWidth >= 1366) {
        $.id("buttonControls").style.display = "none";
        canvas.style.height = window.screen.availHeight / 1.5 + "px";
    }
}

function createPlayer(tile, resetHp) {
    player = new Player(tile, resetHp, 0);
}

function createEnemies() {
    enemyArr = new Array();
    let numOfEnemies = level;
    for (let i = 0; i < numOfEnemies; i++) {
        enemyArr.push(new Enemy(randomTile(), i));
    }    
}

function moveEnemies() {
    enemyArr.forEach(enemy => {
        if (enemy.alive) {
            let rad = 50;
            console.log(enemy);
    
            // Is player close
            let playerUp = player.y + rad*2 <= enemy.y;
            let playerDown = player.y - rad >= enemy.y;
            let playerLeft = player.x + rad*2 <= enemy.x;
            let playerRight = player.x - rad >= enemy.x;
    
            let playerNw = playerUp && playerLeft;
            let playerNe = playerUp && playerRight;
            let playerSe = playerDown && playerRight;
            let playerSw = playerDown && playerLeft;
    
            // Is another enemy close
            let enemyUp = true;
            let enemyDown = true;
            let enemyLeft = true;
            let enemyRight = true;
    
            enemyUp = eUp(enemy);
            enemyDown = eDown(enemy);
            enemyLeft = eLeft(enemy);
            enemyRight = eRight(enemy);
    
            let enemyNw = enemyUp && enemyLeft;
            let enemyNe = enemyUp && enemyRight;
            let enemySe = enemyDown && enemyRight;
            let enemySw = enemyDown && enemyLeft;
    
            // Move to possible direction 
            if (!enemy.playerNear) {
                if (!enemy.obstacleUp && playerUp && !enemyUp) {
                    enemy.moveUp();
                }
                else if (!enemy.obstacleDown && playerDown && !enemyDown) {
                    enemy.moveDown();
                }   
                else if (!enemy.obstacleLeft && playerLeft && !enemyLeft) {
                    enemy.moveLeft();
                }
                else if (!enemy.obstacleRight && playerRight && !enemyRight) {
                    enemy.moveRight();
                }
                else if (!enemy.obstacleNw && playerNw && !enemyNw) {
                    enemy.moveNw();
                }
                else if (!enemy.obstacleNe && playerNe && !enemyNe) {
                    enemy.moveNe();
                }  
                else if (!enemy.obstacleSe && playerSe && !enemySe) {
                    enemy.moveSe();
                }
                else if (!enemy.obstacleSw && playerSw && !enemySw) {
                    enemy.moveSw(); 
                }
            }
        }
    });
}

function eUp(enemy) {
    let rad = 50;
    enemyArr.forEach(e => {
        if (e.alive) {
            return e.y + rad*2 <= enemy.y;
        }
    });
    return false;
}

function eDown(enemy) {
    let rad = 50;
    enemyArr.forEach(e => {
        if (e.alive) {
            return e.y - rad >= enemy.y;
        }
    });
    return false;
}

function eLeft(enemy) {
    let rad = 50;
    enemyArr.forEach(e => {
        if (e.alive) {
            return e.x + rad*2 <= enemy.x;
        }
    });
    return false;
}

function eRight(enemy) {
    let rad = 50;
    enemyArr.forEach(e => {
        if (e.alive) {
            return e.x - rad >= enemy.x;
        }
    });
    return false;
}

function setUpControls() {

    // Button movement
    upButton = $.id("upButton");
    downButton = $.id("downButton");
    leftButton = $.id("leftButton");
    rightButton = $.id("rightButton");
    nwButton = $.id("nwButton");
    neButton = $.id("neButton");
    seButton = $.id("seButton");
    swButton = $.id("swButton");
    enterButton = $.id("enterButton");
    enterButton.style.display = "none";
    let keyPresses = 0;
    let keyPressed = false;

    upButton.addEventListener("mousedown", e => {
        player.moveUp(); 
        moveEnemies();
    });

    downButton.addEventListener("mousedown", e => {
        player.moveDown();
        moveEnemies();
    });

    leftButton.addEventListener("mousedown", e => {
        player.moveLeft();
        moveEnemies();
    });

    rightButton.addEventListener("mousedown", e => {
        player.moveRight();
        moveEnemies();
    });

    nwButton.addEventListener("mousedown", e => {
        player.moveNw();
        moveEnemies();
    });

    neButton.addEventListener("mousedown", e => {
        player.moveNe();
        moveEnemies();
    });

    seButton.addEventListener("mousedown", e => {
        player.moveSe();
        moveEnemies();
    });

    swButton.addEventListener("mousedown", e => {
        player.moveSw();
        moveEnemies();
    });

    enterButton.addEventListener("mousedown", e => {
        resetGame();
    });

    window.addEventListener("mouseup", e => {
    });

    // Keyboard arrows movement
    document.onkeydown = e => {
        console.debug(e.keyCode);

        if (player.alive) {

            if (keyPresses > 20) {
                keyPresses = 0;
                clearEventText();
            } 
            // Up
            if (e.keyCode == "38" || e.keyCode == '104' || e.keyCode == '56' && !keyPressed) {
                keyPressed = true;
                keyPresses++;
                player.moveUp();
                enemyTurn();
            }
            // Down
            else if (e.keyCode == "40" || e.keyCode == '98' || e.keyCode == '50' && !keyPressed) {
                keyPressed = true;
                keyPresses++;
                player.moveDown();
                enemyTurn();
            }
            // Left
            else if (e.keyCode == "37" || e.keyCode == '100' || e.keyCode == '52' && !keyPressed) {
                keyPressed = true;
                keyPresses++;
                player.moveLeft();
                enemyTurn();
            }
            // Right
            else if (e.keyCode == "39" || e.keyCode == '102' || e.keyCode == '54' && !keyPressed) {
                keyPresses++;
                player.moveRight();
                enemyTurn();
            }
            // North West
            else if (e.keyCode == '55' || e.keyCode == '103' && !keyPressed) {
                keyPressed = true;
                keyPresses++;
                player.moveNw();
                enemyTurn();
            }
            // North East
            else if (e.keyCode == '57' || e.keyCode == '105' && !keyPressed) {
                keyPressed = true;
                keyPresses++;
                player.moveNe();
                enemyTurn();
            }
            // South East
            else if (e.keyCode == '51' || e.keyCode == '99' && !keyPressed) {
                keyPressed = true;
                keyPresses++;
                player.moveSe();
                enemyTurn();
            }
            // South West
            else if (e.keyCode == '49' || e.keyCode == '97' && !keyPressed) {
                keyPressed = true;
                keyPresses++;
                player.moveSw();
                enemyTurn();
            }
            // Skip
            else if (e.keyCode == '53' || e.keyCode == '190' && !keyPressed) {
                keyPressed = true;
                keyPresses++;
                enemyTurn();
            }
            // Pick up
            else if (e.keyCode == '71' && !keyPressed) {
                keyPressed = true;
                keyPresses++;
                player.pickUp();
            }
            // Skill screen
            else if (e.keyCode == '77' && !keyPressed) {
                keyPressed = true;
                keyPresses++;
                if (skillScreenOpen) {
                    skillScreenOpen = false;
                }  
                else {
                    skillScreenOpen = true;
                }
            }
        }
        if (!player.alive && !keyPressed) {
            if (e.keyCode == "13") {
                keyPressed = true;
                resetGame();
            }
        }
    };

    document.onkeyup = e => {
        keyPressed = false;
    }
}

function enemyTurn() {
    player.clearObstacles();
    player.clearEnemies();
    moveEnemies();
    enemyArr.forEach(enemy => {
        enemy.clearObstacles();
    });
    console.log(player);
}

function clearEventText() {
    eventText = "";
    eventText2 = "";
    drawGuiCanvas();
}



function createTiles() {
    let tiles = new Array();
    let id = 0;
    for (let j = 0; j < gameAreaY; j += 50) {
        for (let i = 0; i < gameAreaX; i += 50) {
            tiles.push(new Tile(i, j, id));   
            id++;     
        }
    }
    return tiles;
}

function createSpecificTile(x, y, id) {
    specificTile(x, y);
    let tile = new Tile(x, y, id);
    tiles.push(tile);
    return tile;   
}

function randomTile() {
    let tile = tiles[$.rng(0, tiles.length)];
    tiles.splice(tiles.indexOf(tile), 1);
    return tile;
}

function specificTile(x, y) {
    tiles.forEach(tile => {
        if (tile.x == x && tile.y == y) {
            let currentTile = tile;
            tiles.splice(tiles.indexOf(currentTile), 1);
            console.log(currentTile);
            return currentTile;
        }
    });
}

function removeTile(x, y) {
    tiles.forEach(tile => {
        if (tile.x == x && tile.y == y) {
            let currentTile = tile;
            tiles.splice(tiles.indexOf(currentTile), 1);
        }
    });
}

function createWalls() {
    wallArr = new Array();
    let numOfWalls = 25 + Math.floor(Math.random() * Math.floor(25));;
    for (let i = 0; i < numOfWalls; i++) {
        wallArr.push(new Wall(randomTile(), i, level));
    }
}

function createCoins() {
    coinArr = new Array();
    let numOfCoins = 10;
    coinsPickedUp = 0;
    coinAmount = numOfCoins;
    for (let i = 0; i < numOfCoins; i++) {
        coinArr.push(new Coin(randomTile(), i));
    }
}

// Item rng
function createPotions() {
    potionArr = new Array();
    let numOfPotions = $.rngSpawn();
    console.log(numOfPotions);
    for (let i = 0; i < numOfPotions; i++) {
        potionArr.push(new Potion(randomTile(), i));
    }
}

function createWeapons() {
    weaponArr = new Array();
    let numOfWeapons = $.rngSpawn();
    console.log(numOfWeapons);
    for (let i = 0; i < numOfWeapons; i++) {
        weaponArr.push(new Weapon(randomTile(), i));
    }
}

function createTorsoArmors() {
    torsoArmorArr = new Array();
    let numOfTorsoArmor = $.rngSpawn();
    console.log(numOfTorsoArmor);
    for (let i = 0; i < numOfTorsoArmor; i++) {
        torsoArmorArr.push(new TorsoArmor(randomTile(), i));
    }
}

function createExit() {
    exit = new Exit(randomTile(), false, level);
}

function startGame() {
    tiles = createTiles();
    createPlayer(randomTile(), 50);
    removeTilesAround(player);
    createExit();
    createWalls();
    createCoins();
    createPotions();
    createWeapons();
    createTorsoArmors();
    createEnemies();
    clearEventText();
}

function nextLevel() {
    exit.alive = false;
    level++;
    tiles = createTiles();
    specificTile(player.x, player.y);
    removeTilesAround(player);
    createExit();
    createWalls();
    createCoins();
    createPotions();
    createWeapons();
    createTorsoArmors();
    createEnemies();
    clearEventText();
}

function resetGame() {
    if (!player.alive) {
        enterButton.style.display = "none";    
        exit.alive = false;
        level = 1;
        score = 0;
        startGame();
    }
}

function removeTilesAround(object) {
    
    removeTile(object.x -50, object.y -50);
    removeTile(object.x, object.y -50);
    removeTile(object.x + 50, object.y -50);

    removeTile(object.x - 50, object.y);
    removeTile(object.x + 50, object.y);
    
    removeTile(object.x -50, object.y +50);
    removeTile(object.x, object.y +50);
    removeTile(object.x + 50, object.y +50);
}

window.onload = init;
