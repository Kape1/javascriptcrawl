class Coin {

    constructor(tile, id) {
        this.id = "coin" + id;
        this.type = "coin";
        let rad = 25;
        this.width = rad;
        this.height = rad;
        this.x = tile.x;
        this.y = tile.y;
        this.color = "yellow";
        this.alive = true;
        let coinImg = new Image();
        let imgArr = ["gold_pile_1", "gold_pile_1", "gold_pile_1", "gold_pile_5", "gold_pile_5", "gold_pile_16"];
        coinImg.src = pickupsUrl + imgArr[Math.floor(Math.random() * imgArr.length)] + ".png";
        this.image = coinImg;
    }
}