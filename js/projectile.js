class Projectile {

    constructor(tile) {
        let rad = 50;
        this.width = rad;
        this.height = rad;
        this.x = tile.x;
        this.y = tile.y;
        this.color = "red";
        this.alive = true;
    }
}