
function detectCollisions(object) {
    
    // Coin & exit collision
    if (object.type == "player") {
        damageCollision();
        enemyOnTopOfPlayer();
        detectDirectHit(object, enemyArr);
        detectDirectHit(object, coinArr);
        detectExit();
    }

    // Object to enemy collision
    collisionUp(object, enemyArr);
    collisionDown(object, enemyArr);
    collisionRight(object, enemyArr);
    collisionLeft(object, enemyArr);
    collisionNw(object, enemyArr);
    collisionNe(object, enemyArr);
    collisionSe(object, enemyArr);
    collisionSw(object, enemyArr);

    if (object.type == "enemy") {
        detectDirectHit(object, enemyArr);
    }

    // Wall collisions
    collisionUp(object, wallArr);
    collisionDown(object, wallArr);
    collisionRight(object, wallArr);
    collisionLeft(object, wallArr);
    collisionNw(object, wallArr);
    collisionNe(object, wallArr);
    collisionSe(object, wallArr);
    collisionSw(object, wallArr);
}

function collisionUp(object, arr) {
    if (object.y == 0) {
        object.obstacleUp = true;
    }
    else {
        arr.forEach(obstacle => {  
            if (obstacle.alive && 
                object.y == obstacle.y + obstacle.height &&
                object.x == obstacle.x) {
                    object.obstacleUp = true;
                    if (object.type == "player" && obstacle.type == "enemy" && obstacle.alive) {
                        object.enemyUp = true;
                        obstacle.playerDown = true;
                        obstacle.playerNear = true;
                    }
            }
        });
    }
}

function collisionDown(object, arr) {
    if (object.y + object.height == gameAreaY) {
        object.obstacleDown = true;
    }
    else {
        arr.forEach(obstacle => {  
            if (obstacle.alive && 
                object.y + object.height == obstacle.y &&
                object.x == obstacle.x) {
                    object.obstacleDown = true;
                    if (object.type == "player" && obstacle.type == "enemy" && obstacle.alive) {
                        object.enemyDown = true;
                        obstacle.playerUp = true;
                        obstacle.playerNear = true;
                    }
            }
        });
    }
}

function collisionLeft(object, arr) {
    if (object.x == 0) {
        object.obstacleLeft = true;
    }
    else {
        arr.forEach(obstacle => {  
            if (obstacle.alive && 
                object.x == obstacle.x + obstacle.width &&
                object.y == obstacle.y) {
                    object.obstacleLeft = true;
                    if (object.type == "player" && obstacle.type == "enemy" && obstacle.alive) {
                        object.enemyLeft = true;
                        obstacle.playerRight = true;
                        obstacle.playerNear = true;
                    }
            }
        });
    }
}

function collisionRight(object, arr) {
    if (object.x + object.width == gameAreaX) {
        object.obstacleRight = true;
    }
    else {
        arr.forEach(obstacle => {  
            if (obstacle.alive && 
                object.x + object.width == obstacle.x &&
                object.y == obstacle.y) {
                    object.obstacleRight = true;
                    if (object.type == "player" && obstacle.type == "enemy" && obstacle.alive) {
                        object.enemyRight = true;
                        obstacle.playerLeft = true;
                        obstacle.playerNear = true;
                    }
            }
        });
    }
}

function collisionNw(object, arr) {
    if (object.y == 0 || object.x == 0) {
        object.obstacleNw = true;
    }
    else {
        arr.forEach(obstacle => {  
            if (obstacle.alive && 
                object.x == obstacle.x + obstacle.width &&
                object.y == obstacle.y + obstacle.height) {
                    object.obstacleNw = true;
                    if (object.type == "player" && obstacle.type == "enemy" && obstacle.alive) {
                        object.enemyNw = true;
                        obstacle.playerSe = true;
                        obstacle.playerNear = true;
                    }
            }
        });
    }
}

function collisionNe(object, arr) {
    if (object.y == 0 || object.x + object.width == gameAreaX) {
        object.obstacleNe = true;
    }
    else {
        arr.forEach(obstacle => {  
            if (obstacle.alive && 
                object.x + object.width == obstacle.x &&
                object.y == obstacle.y + obstacle.height) {
                    object.obstacleNe = true;
                    if (object.type == "player" && obstacle.type == "enemy" && obstacle.alive) {
                        object.enemyNe = true;
                        obstacle.playerSw = true;
                        obstacle.playerNear = true;
                    }
            }
        });
    }
}

function collisionSe(object, arr) {
    if (object.y + object.height == gameAreaY ||
        object.x + object.width == gameAreaX) {
        object.obstacleSe = true;
    }
    else {
        arr.forEach(obstacle => {  
            if (obstacle.alive && 
                object.x + object.width == obstacle.x &&
                object.y + object.height == obstacle.y) {
                    object.obstacleSe = true;
                    if (object.type == "player" && obstacle.type == "enemy" && obstacle.alive) {
                        object.enemySe = true;
                        obstacle.playerNw = true;
                        obstacle.playerNear = true;
                    }
            }
        });
    }
}

function collisionSw(object, arr) {
    if (object.y + object.height == gameAreaY || object.x == 0) {
        object.obstacleSw = true;
    }
    else {
        arr.forEach(obstacle => {  
            if (obstacle.alive && 
                object.x == obstacle.x + obstacle.width &&
                object.y + object.height == obstacle.y) {
                    object.obstacleSw = true;
                    if (object.type == "player" && obstacle.type == "enemy" && obstacle.alive) {
                        object.enemySw = true;
                        obstacle.playerNe = true;
                        obstacle.playerNear = true;
                    }
            }
        });
    }
}

function enemyOnTopOfPlayer() {
    enemyArr.forEach(enemy => {
        if (player.x == enemy.x && player.y == enemy.y) {
            registerDamage();
        }
    });
}

function detectDirectHit(object, arr) {
    arr.forEach(obstacle => {  
        if (obstacle.alive &&
            object.x < obstacle.x + obstacle.width &&
            object.x + object.width > obstacle.x &&
            object.y < obstacle.y + obstacle.height &&
            object.height + object.y > obstacle.y) {
                if (obstacle.type == "coin") {
                    pickupCoin(obstacle);
                }
                else if (obstacle.type == "potion") {
                    pickupPotion(obstacle);
                }
                else if (obstacle.type == "enemy" && object.id != obstacle.id) {
                    displaceEnemy(object, obstacle);
                }
                else if (obstacle.type == "weapon") {
                    pickupWeapon(obstacle);
                }
                else if (obstacle.type == "torsoArmor") {
                    pickupTorsoArmor(obstacle);
                }
        }
    });
}

// Displays another object if they are in the same square
function displaceEnemy(object, enemy) {
    if (!object.obstacleUp) {
        enemy.moveUp();
    }
    else if (!object.obstacleDown) {
        enemy.moveDown();
    }
    else if (!object.obstacleLeft) {
        enemy.moveLeft();
    }
    else if (!object.obstacleRight) {
        enemy.moveRight();
    }
    else if (!object.obstacleNw) {
        enemy.moveNw();
    }
    else if (!object.obstacleNe) {
        enemy.moveNe();
    }
    else if (!object.obstacleSe) {
        enemy.moveSe();
    }
    else if (!object.obstacleSw) {
        enemy.moveSw();
    }
}

function detectExit() {
    if (exit.alive &&
        player.x < exit.x + exit.width &&
        player.x + player.width > exit.x &&
        player.y < exit.y + exit.height &&
        player.height + player.y > exit.y) {
            console.log("Exit reached");
            nextLevel();
    }
}

function damageCollision() {
    if (!player.enemyNear && (player.enemyUp || player.enemyDown || player.enemyLeft || player.enemyRight || 
        player.enemyNw || player.enemyNe || player.enemySe || player.enemySw)) {
            player.enemyNear = true;
            registerDamage();
    }
    else {     
        enemyArr.forEach(enemy => {
            enemy.playerNear = false;
        });
    }
}

// Combat calculations
function registerDamage() {
    if (player.alive) {
        enemyArr.forEach(enemy => {
            if (enemy.playerNear) {

                let skillIncrement = 0.5;

                // For hit graphics
                let red = "red";
                let rad = 25;

                // Event texts
                let playerText = player.name + " missed the " + enemy.name + "!  "
                let enemyText = enemy.name + " missed " + player.name + "!  "

                // Player's weapon stats
                //let damageBonus = 0;
                let damage = player.equippedWeapon.damage;

                // Damage & to hit bonus
                damageBonus = calcDamageBonus();

                damage += damageBonus;

                // Player attack enemy
                if (player.equippedWeapon.hitChance > $.rng(0,2)) {
                    
                    // Hit graphics                
                    ctx.fillStyle = red;
                    ctx.fillRect(enemy.x + rad/2, enemy.y + rad/2, rad, rad);  
                    ctx.fillRect(enemy.x + rad/2, enemy.y + rad/2, rad, rad);
                    ctx.fillRect(enemy.x + rad/2, enemy.y + rad/2, rad, rad);

                    // Player hits enemy
                    damage = $.round(damage);
                    enemy.hp -= damage;
                    enemy.hp = $.round(enemy.hp);
                    
                    console.log("Player's damage: " + damage);
                    
                    // Weapon xp
                    switch (player.equippedWeapon.category) {
                        case "Light Weapon":
                            player.lightWeaponSkill+= skillIncrement;
                            break;

                        case "Heavy Weapon":
                            player.heavyWeaponSkill+= skillIncrement;
                            break;
                    }

                    // Weapon loses durability
                    if (player.equippedWeapon.durability > 0) {
                        player.equippedWeapon.durability--;
                    }

                    if (player.equippedWeapon.durability <= 0 && player.equippedWeapon.name != "") {
                        player.equipWeapon(new Weapon(null));
                    }

                    // Change event text to hit
                    playerText = player.name + " dealt " + damage +" dmg to " + enemy.name + "!  "

                    // Weapon drop image names            
                    let dagger = "dagger_new";
                    let club = "club_new";
                    let longSword = "long sword_1_new";
                    let heavyAxe = "heavy axe_4";
                        
                    

                // Enemy attacks player
                if (enemy.alive && enemy.hitChance - player.equippedTorsoArmor.dodge > $.rng(0, 2)) {

                    // Hit graphics                
                    ctx.fillStyle = red;
                    ctx.fillRect(player.x + rad/2, player.y + rad/2, rad, rad);  
                    ctx.fillRect(player.x + rad/2, player.y + rad/2, rad, rad);  
                    ctx.fillRect(player.x + rad/2, player.y + rad/2, rad, rad);     

                    // Player's armor stats        
                    let armor = 0;
                    //let torsoArmorBonus = 0;
                    let torsoArmor = player.equippedTorsoArmor.armor;

                    // Player's torso armor bonus
                    let torsoArmorBonus = calcTorsoArmorBonus();

                    torsoArmor += torsoArmorBonus;
                    armor += torsoArmor;

                    // Enemy's real damage after armor and skill modifiers
                    let enemysDamage = $.round(enemy.damage - armor);

                    if (enemysDamage < 0) {
                        enemysDamage = 0;
                    }

                    console.log("enemysDamage: " + enemysDamage);

                    // Enemy hits player
                    player.hp -= enemysDamage;
                    player.hp = $.round(player.hp);

                    // Armor xp
                    switch (player.equippedTorsoArmor.category) {
                        case "Light Armor":
                            player.lightArmorSkill += skillIncrement;
                            break;
                        
                        case "Heavy Armor":
                            player.heavyArmorSkill += skillIncrement;
                            break;
                    }

                    // Change event text to hit
                    enemyText = enemy.name + " dealt " + enemysDamage + " dmg to " + player.name + "!  ";
                    
                    // If the player dies
                    if (player.equippedTorsoArmor.durability > 0) {
                        player.equippedTorsoArmor.durability--;
                    }

                    if (player.equippedTorsoArmor.durability <= 0) {
                        player.equipTorsoArmor(new TorsoArmor(null));
                    }

                    if (player.hp <= 0) {
                        player.alive = false;
                    }
                }

                typeEventText(playerText, enemyText);

                    // If the enemy dies
                    if (enemy.hp <= 0) {
                        enemy.alive = false;
                        player.clearObstacles();
                        player.clearEnemies();
                        player.gainXp(enemy.xpGain);

                        let tile = createSpecificTile(enemy.x, enemy.y);

                        switch (enemy.name) {
                            case "Hobgoblin":
                            if ($.rng(0, 3) == 1) {
                                weaponArr.push(new Weapon(tile, 69, dagger));
                            }
                            break;

                            case "Gnoll":
                            if ($.rng(0, 3) == 1) {
                                weaponArr.push(new Weapon(tile, 69, club));
                            }
                            break;

                            case "Orc":
                            if ($.rng(0, 3) == 1) {
                                weaponArr.push(new Weapon(tile, 69, club));
                            }
                            break;

                            case "Axe knight":
                            if ($.rng(0, 3) == 1) {
                                weaponArr.push(new Weapon(tile, 69, heavyAxe));
                            }
                            break;

                            case "Sword knight":
                            if ($.rng(0, 3) == 1) {
                                weaponArr.push(new Weapon(tile, 69, longSword));
                            }
                            break;

                        }
                    }
                }
            }
        });
    }
}

function calcDamageBonus() {
    let damageBonus = 0;
    let wpn = player.equippedWeapon;
    switch (wpn.category) {
        case "Light Weapon":
            damageBonus = wpn.damage / 100 * player.lightWeaponSkill;
            break;

        case "Heavy Weapon":
            damageBonus = wpn.damage / 100 * player.heavyWeaponSkill;
            break;
    }
    return damageBonus;
}

function calcTorsoArmorBonus() {
    let torsoArmorBonus = 0;    
    let armor = player.equippedTorsoArmor;               
    switch (armor.category) {
        case "Light Armor":
            torsoArmorBonus = armor.armor / 100 * player.lightArmorSkill;
            break;
        
        case "Heavy Armor":
            torsoArmorBonus = armor.armor / 100 * player.heavyArmorSkill;
            break;
    }
    return torsoArmorBonus;
}

function typeEventText(text1, text2) {
    let eventTextMaxChar = 40;
    if (eventText.length > eventTextMaxChar || eventText.length > eventTextMaxChar) {
        clearEventText();
    }
    eventText += text1;
    eventText2 += text2;
}

function pickupCoin(coin) {
    coin.alive = false;
    score++;
    coinsPickedUp++;
    if (coinsPickedUp >= coinAmount - 2) {
        if (!exit.alive) {
            exit.alive = true;
        }
    }
}

function pickupPotion(potion) {
    potion.alive = false;
    player.gainHp(potion.hpGain);
    typeEventText("You drank a potion of " + potion.name + " and gained " + potion.hpGain + " hp.", "");
}

function pickupWeapon(weapon) {
    weapon.alive = false;
    player.equipWeapon(weapon);
    typeEventText("You equipped a " + weapon.name + ".", "");
}

function pickupTorsoArmor(torsoArmor) {
    torsoArmor.alive = false;
    player.equipTorsoArmor(torsoArmor);
    typeEventText("You equipped a " + torsoArmor.name + ".", "");
}