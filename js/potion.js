class Potion {

    constructor(tile, id) {
        this.id = "potion" + id;
        this.type = "potion";
        this.name = "health";
        this.hpGain = 50;
        let rad = 25;
        this.width = rad;
        this.height = rad;
        this.x = tile.x;
        this.y = tile.y;
        this.alive = true;
        let potionImg = new Image();
        let imgArr = ["potion_bubbly"];
        potionImg.src = pickupsUrl + imgArr[Math.floor(Math.random() * imgArr.length)] + ".png";
        this.image = potionImg;
    }
}