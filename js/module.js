"use strict";
         
// urls for images
let imgUrl = "../images/";
let wpnUrl = "../images/weapons/";
let torsoUrl = "../images/torso/";
let enemiesUrl = "../images/enemies/";
let pickupsUrl = "../images/pickups/";
let wallUrl = "../images/walls/";

// Colors
let white = "white";
let red = "crimson";
let gold = "gold";
let black = "black";
let orange = "orange";

let $ = (function() {
    return {
        id: function(id) {
            return document.getElementById(id);
        },
        listener: function(id, event, func) {
            return document.getElementById(id).addEventListener(event, func);
        },
        rng: function(min, max) {
            return min + Math.floor(Math.random() * max);
        },
        rngSpawn: function() {
            let max = level;
            if (level >= 5) {
                max = 5;
            }
            return Math.ceil(this.rng(0, this.rng(0, 5 + max)));
        },
        round: function(number) {
            return Math.round(number * 10 ) / 10;
        }
    }
})();