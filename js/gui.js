// Draw + Game loop
function draw() {
    // Background
    ctx.fillStyle = backgroundColor;
    ctx.fillRect(0, 0, gameAreaX, gameAreaY);
    gui.fillStyle = backgroundColor;
    gui.fillRect(0, 0, guiX, guiY);

    // Draw walls
    wallArr.forEach(wall => {
        ctx.drawImage(wall.image, wall.x, wall.y);
    });

    // Draw coins
    coinArr.forEach(coin => {
        if (coin.alive) {
            ctx.drawImage(coin.image, coin.x, coin.y);
        }
    });

    // Draw Potions
    potionArr.forEach(potion => {
        if (potion.alive) {
            ctx.drawImage(potion.image, potion.x, potion.y);
        }
    });

    // Draw Weapons
    weaponArr.forEach(weapon => {
        if (weapon.alive) {
            ctx.drawImage(weapon.groundImage, weapon.x, weapon.y);
            console.debug(weapon.name);
        }
    });

    // Draw Torso armors
    torsoArmorArr.forEach(torsoArmor => {
        if (torsoArmor.alive) {
            ctx.drawImage(torsoArmor.groundImage, torsoArmor.x, torsoArmor.y);
        }
    });

    // Draw exit 
    if (exit.alive) { 
        ctx.drawImage(exit.image, exit.x, exit.y);
    }

    // Draw player"
    if (player.alive) {
        ctx.fillStyle = player.baseColor;
        ctx.fillRect(player.x, player.y, player.width, player.height);
        ctx.drawImage(player.baseImage, player.x, player.y);

        // Player's equipment
        // Weapon
        ctx.drawImage(player.equippedWeapon.equipImage, player.x, player.y);

        // Torso armor
        ctx.drawImage(player.equippedTorsoArmor.equipImage, player.x, player.y);
    }
 
    // Draw enemies    
    enemyArr.forEach(enemy => {
        if (enemy.alive) {
            ctx.fillStyle = enemy.baseColor;
            ctx.fillRect(enemy.x, enemy.y, enemy.width, enemy.height);
            ctx.drawImage(enemy.image, enemy.x, enemy.y);
        }
    });

    // Events gui
    drawGuiCanvas();

    // GUI
    ctx.fillStyle = red;
    ctx.font = "20px Arial";

    // Level
    ctx.fillText("Dungeon: " + level, 25, 25);

    // Score
    ctx.fillText("Score: " + score, gameAreaX - 150, 25);

    // Skill screen
    if (player.alive && skillScreenOpen) {
        let textXpos = gameAreaX / 3 - 230;
        let textYpos = gameAreaY / 5;
        let textXpos2 = textXpos + 400; 
        let headerFont = "50px arial";
        let textFont = "30px arial";

        // Player's level
        ctx.fillStyle = white;
        ctx.font = headerFont;
        ctx.fillText("LEVEL: " + player.level + "\t\tXP: " + player.xp + "/" + player.xpReq,  textXpos, textYpos - 60);

        // Skills
        ctx.fillStyle = white;
        ctx.font = headerFont;
        ctx.fillText("SKILLS", textXpos, textYpos);
        ctx.fillStyle = red;
        ctx.font = textFont;
        ctx.fillText("Light weapons: " + Math.floor(player.lightWeaponSkill), textXpos, textYpos + 80);
        ctx.fillText("Heavy weapons: " + Math.floor(player.heavyWeaponSkill), textXpos, textYpos + 150);
        ctx.fillText("Light armor: " + Math.floor(player.lightArmorSkill), textXpos, textYpos + 220);
        ctx.fillText("Heavy armor: " + Math.floor(player.heavyArmorSkill), textXpos, textYpos + 290);
       
        // Bonus
        ctx.fillStyle = gold;
        ctx.font = textFont;
        if (player.equippedWeapon.category != "") {
            ctx.fillText(player.equippedWeapon.category + " damage bonus: " + $.round(calcDamageBonus()), textXpos, textYpos + 360);
        }
        if (player.equippedTorsoArmor.category != "") {
            ctx.fillText(player.equippedTorsoArmor.category + " bonus: " + $.round(calcTorsoArmorBonus()), textXpos, textYpos + 430);
        }

        // Equipped
        ctx.fillStyle = white;
        ctx.font = headerFont;
        ctx.fillText("EQUIPPED", textXpos2, textYpos);
        ctx.fillStyle = red;
        ctx.font = textFont;
        if (player.equippedWeapon.category != "") {
            ctx.fillText("Weapon: " + player.equippedWeapon.name, textXpos2, textYpos + 80);
            ctx.fillText("Category: " + player.equippedWeapon.category, textXpos2, textYpos + 150);
        }
        if (player.equippedTorsoArmor.category != "") {
            ctx.fillText("Torso: " + player.equippedTorsoArmor.name, textXpos2, textYpos + 220);
            ctx.fillText("Category: " + player.equippedTorsoArmor.category, textXpos2, textYpos + 290);
        }
    }

    // Game over text
    if (!player.alive) {
        enterButton.style.display = "block";
        let textXpos = gameAreaX / 2 - 250
        let textYpos = gameAreaY / 3;
        ctx.fillStyle = red;
        ctx.font = "100px Arial";
        ctx.fillText("YOU DIED", textXpos, textYpos);
        ctx.fillStyle = red;
        ctx.font = "50px Arial";
        ctx.fillText("Final score: " + score, textXpos + 75, textYpos + 80);
        ctx.fillText("Press enter to try again.", textXpos, textYpos + 150);
    }

    detectCollisions(player);
    enemyArr.forEach(enemy => {
        detectCollisions(enemy);
    });
}

function drawGuiCanvas() {
    let hpPosX = 5;
    gui.fillStyle = white;
    gui.font = "20px Arial";
    let playerHP = " | HP: " + player.hp;
    let playerAtt = " | Att: " + player.equippedWeapon.damage;
    let playerHit = " | H%: " + player.equippedWeapon.hitPercent;
    let playerWeaponDur = 0;
    let playerTorsoAc = 0;
    let playerTorsoDur = 0;
    let playerDodge = 0;

    if (player.equippedWeapon.durability > 0) {
        playerWeaponDur = " | Dur: " + player.equippedWeapon.durability;
    }

    if (player.equippedTorsoArmor.durability > 0) {
        playerTorsoAc = " | AC: " + player.equippedTorsoArmor.armor;
        playerTorsoDur = " | Dur:" + player.equippedTorsoArmor.durability;
        playerDodge = " | Ddg: " + player.equippedTorsoArmor.dodgePercent;
    } 
    

    // Player's hp color changes
    if (player.hp <= player.maxHp / 2) {
        gui.fillStyle = orange;
    }
    if (player.hp <= player.maxHp / 4) {
        gui.fillStyle = red;
    }
    gui.fillText(playerHP, hpPosX, 20);

    // Player's attack
    gui.fillStyle = white;
    gui.fillText(playerAtt, hpPosX + 100, 20);
    gui.fillText(playerHit, hpPosX + 180, 20);

    // Weapon dur
    if (player.equippedWeapon.durability > 0) {
        if (player.equippedWeapon.durability <= player.equippedWeapon.maxDur / 2) {
            gui.fillStyle = orange;
        }    
        if (player.equippedWeapon.durability <= player.equippedWeapon.maxDur / 4) {
            gui.fillStyle = red;
        }    
        gui.fillText(playerWeaponDur, hpPosX + 260, 20);
    }

    // Armor text
    gui.fillStyle = white;
    if (player.equippedTorsoArmor.durability > 0) {
        gui.fillText(playerTorsoAc, hpPosX + 360, 20);
        if (player.equippedTorsoArmor.durability <= player.equippedTorsoArmor.maxDur / 2) {
            gui.fillStyle = orange;
        }
        if (player.equippedTorsoArmor.durability <= player.equippedTorsoArmor.maxDur / 4) {
            gui.fillStyle = red;
        }
        gui.fillText(playerTorsoDur, hpPosX + 440, 20);
        gui.fillStyle = white;
        gui.fillText(playerDodge, hpPosX + 520, 20);
    }

    gui.fillText(eventText, 10, 50);
    
    gui.fillStyle = red;
    gui.font = "20px Arial";
    gui.fillText(eventText2, 10, 80);
}
