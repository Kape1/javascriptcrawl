class Player {

    constructor(tile, hp, id) {

        this.id = "player" + id;
        
        // Stats
        this.name = "You";
        this.type = "player";
        this.alive = true;
        this.maxHp = 100;
        this.hp = 100;
        this.speed = 50;

        // Player level
        this.level = 1;
        this.xp = 0;
        this.xpReq = 10;

        // Weapon skills
        this.lightWeaponSkill = 0;
        this.heavyWeaponSkill = 0;

        // Armor skills
        this.lightArmorSkill = 0;        
        this.heavyArmorSkill = 0;

        let baseRad = 50;
        let avatarRad = 40; 

        // Base image
        let playerImg = new Image(); 
        playerImg.src = imgUrl + "human_male.png";
        this.baseImage = playerImg;

        // Base Weapon
        let unarmedWeapon = new Weapon(null);
        this.equippedWeapon = unarmedWeapon;
        
        // Base torso armor
        let noTorsoArmor = new TorsoArmor(null);
        this.equippedTorsoArmor = noTorsoArmor;

        this.x = tile.x;
        this.y = tile.y;
        this.width = baseRad;
        this.height = baseRad;
        this.baseColor = "transparent";

        this.avatarW = avatarRad;
        this.avatarH = avatarRad;

        this.obstacleUp = false;
        this.obstacleDown =false;
        this.obstacleLeft = false;
        this.obstacleRight = false; 
        this.obstacleNw = false;
        this.obstacleNe = false;
        this.obstacleSe = false;
        this.obstacleSw = false;

        this.enemyNear = false;
        this.enemyUp = false;
        this.enemyDown =false;
        this.enemyLeft = false;
        this.enemyRight = false; 
        this.enemyNw = false;
        this.enemyNe = false;
        this.enemySe = false;
        this.enemySw = false;
    }   

    moveUp() {
        if (!this.obstacleUp && !this.enemyUp) {
            this.y -= this.speed;
        }
    }
    
    moveDown() {
        if (!this.obstacleDown && !this.enemyDown) {
            this.y += this.speed;
        }
    }
    
    moveLeft() {
        if (!this.obstacleLeft && !this.enemyLeft) {
            this.x -= this.speed;
        }
    }
    
    moveRight() {
        if (!this.obstacleRight && !this.enemyRight){
            this.x += this.speed;
        }
    }
    
    moveNw() {
        if (!this.obstacleNw && !this.enemyNw){
            this.y -= this.speed;
            this.x -= this.speed;
        }
    }
    
    moveNe() {
        if (!this.obstacleNe && !this.enemyNe){
            this.y -= this.speed;
            this.x += this.speed;
        }
    }
    
    moveSe() {
        if (!this.obstacleSe && !this.enemySe){
            this.y += this.speed;
            this.x += this.speed;
        }
    }
    
    moveSw() {
        if (!this.obstacleSw && !this.enemySw){
            this.y += this.speed;
            this.x -= this.speed;
        }
    }

    pickUp() {
        detectDirectHit(player, potionArr);
        detectDirectHit(player, weaponArr);
        detectDirectHit(player, torsoArmorArr);
    }
    
    // No obstacles next to the player.
    clearObstacles() {
        player.obstacleUp = false;
        player.obstacleDown = false;
        player.obstacleLeft = false;
        player.obstacleRight = false;
        player.obstacleNw = false;
        player.obstacleNe = false;
        player.obstacleSe = false;
        player.obstacleSw = false;
    }

    // No enemies next to the player.
    clearEnemies() {
        this.enemyNear = false;
        this.enemyUp = false;
        this.enemyDown = false;
        this.enemyLeft = false;
        this.enemyRight = false;
        this.enemyNw = false;
        this.enemyNe = false;
        this.enemySe = false;
        this.enemySw = false;
    }

    gainHp(hp) {
        if (this.hp + hp > this.maxHp) {
            this.hp = this.maxHp;
        }
        else {
            this.hp += hp;
        }
    }

    equipWeapon(weapon) {
        this.equippedWeapon = weapon;
    }

    equipTorsoArmor(torsoArmor) {
        this.equippedTorsoArmor = torsoArmor;
    }

    gainXp(xp) {
        this.xp += xp;
        typeEventText(xp + "xp gained! ", "");
        // Level up
        if (this.xp >= this.xpReq) {
            typeEventText("Level up! ", "");
            this.xpReq += this.xpReq;
            this.xp = 0;
            this.level += 1;
            this.maxHp += 10;
        }
    }
}