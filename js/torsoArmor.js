class TorsoArmor {

    constructor(tile, id) {
        this.id = "torsoArmor" + id;
        this.type = "torsoArmor";
        let gImg = new Image();
        let eImg = new Image();
        
        // No armor
        if (!tile) {
            this.category = "";
            this.name = this.category;
            this.armor = 0;
            this.maxDur = 0;
            this.durability = this.maxDur;                    
            this.dodgePercent = 0;
            this.dodge = this.dodgePercent / 100;
            gImg.src = torsoUrl + "unarmed.png";
            this.name = "";
            this.groundImage = gImg;
            this.equipImage = gImg;
        }
        else {
            let imgArr = ["chainmail_1", "platemail_1"];
            let imgName = imgArr[$.rng(0, imgArr.length)];
            this.name = imgName.substring(0, imgName.indexOf("_"));

            // Ground image
            gImg.src = pickupsUrl + imgName + ".png";
            this.groundImage = gImg;

            // Torso image
            eImg.src = torsoUrl + this.name + ".png";
            this.equipImage = eImg;

            // Capitalize the name
            this.name = this.name.charAt(0).toUpperCase() + this.name.slice(1);
            
            switch (this.name) {
                case "Chainmail": 
                    this.category = "Light Armor";
                    this.armor = 2;
                    this.maxDur = 40;
                    this.durability = this.maxDur;
                    this.dodgePercent = 20;
                    this.dodge = this.dodgePercent / 100;
                    break;

                case "Platemail": 
                    this.category = "Heavy Armor";
                    this.armor = 5;
                    this.maxDur = 50;
                    this.durability = this.maxDur;
                    this.dodgePercent = 0;
                    this.dodge = this.dodgePercent / 100;
                    break;
            }

            let rad = 25;
            this.width = rad;
            this.height = rad;
            this.x = tile.x;
            this.y = tile.y;
            this.alive = true;
        }
    }
}